const http = require('http')

const address = "localhost"
const port = 4000

const server = http.createServer((req, res) => {
    res.statusCode = 200

    console.log("Ciao server!")
    
    res.end("Hello World")
})

server.listen(port, address, () => {
    console.log("Sono in ascolto sulla porta 4000")
})