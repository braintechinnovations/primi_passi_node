import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GalileoComponent } from './galileo/galileo.component';
import { LeonardoComponent } from './leonardo/leonardo.component';
import { TeslaComponent } from './tesla/tesla.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "galileo",
    pathMatch: "full"
  },
  {
    path: "galileo",
    component: GalileoComponent
  },
  {
    path: "leonardo",
    component: LeonardoComponent
  },
  {
    path: "tesla",
    component: TeslaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
