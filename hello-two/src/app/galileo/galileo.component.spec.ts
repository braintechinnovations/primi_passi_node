import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GalileoComponent } from './galileo.component';

describe('GalileoComponent', () => {
  let component: GalileoComponent;
  let fixture: ComponentFixture<GalileoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GalileoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalileoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
