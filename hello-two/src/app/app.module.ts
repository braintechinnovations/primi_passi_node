import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GalileoComponent } from './galileo/galileo.component';
import { LeonardoComponent } from './leonardo/leonardo.component';
import { TeslaComponent } from './tesla/tesla.component';
import { ProdottoDetailComponent } from './prodotto-detail/prodotto-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    GalileoComponent,
    LeonardoComponent,
    TeslaComponent,
    ProdottoDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
