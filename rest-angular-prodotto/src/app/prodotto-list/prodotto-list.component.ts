import { Component, OnInit } from '@angular/core';
import { ProdottoService } from '../prodotto.service';

@Component({
  selector: 'app-prodotto-list',
  templateUrl: './prodotto-list.component.html',
  styleUrls: ['./prodotto-list.component.css']
})
export class ProdottoListComponent implements OnInit {

  elenco : any = [];

  constructor(private restProdotto: ProdottoService) { }

  ngOnInit(): void {

    this.restProdotto.findAllProdotti().subscribe(
      (risultato) => {
        this.elenco = risultato
      },
      (error) => {
        console.log(error)
      }
    )

  }



}
