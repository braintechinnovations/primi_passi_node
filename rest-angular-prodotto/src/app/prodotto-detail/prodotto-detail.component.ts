import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Prodotto } from '../prodotto';
import { ProdottoService } from '../prodotto.service';

@Component({
  selector: 'app-prodotto-detail',
  templateUrl: './prodotto-detail.component.html',
  styleUrls: ['./prodotto-detail.component.css']
})
export class ProdottoDetailComponent implements OnInit {

  prodottoId : number | undefined;
  prodottoCodice : string | undefined  = "N.D.";     //Imposto il valore di default
  prodottoNome : string | undefined = "N.D.";
  prodottoDescrizione : string | undefined  = "N.D.";

  showTuttoOk = false;

  prodottoObj : Prodotto | undefined;

  constructor(
    private rottaAttiva: ActivatedRoute,
    private restProdotto: ProdottoService,
    private router: Router
    ) { 
  }

  ngOnInit(): void {

    this.rottaAttiva.params.subscribe(
      (parametri) => {
        console.log(parametri.identificatore)
        this.prodottoId = parametri.identificatore;         //Mi servirà successivamente per la delete

        if(!this.prodottoId)
          return;

        this.restProdotto.findProdottoById(this.prodottoId).subscribe(
          (risultato) => {
            this.prodottoNome = risultato.nome;
            this.prodottoDescrizione = risultato.descrizione;
            this.prodottoCodice = risultato.codice;
            this.prodottoId = risultato.id
          }
        )
      }
    )

  }

  eliminaProdotto(){

    if(!this.prodottoId)
          return;

    this.restProdotto.deleteProdotto(this.prodottoId).subscribe(
      (risultato) => {
        if(risultato)
          this.showTuttoOk = true;

          setTimeout(() => {
            this.router.navigateByUrl('prodotto/list')    //Redirect tramite URL
          }, 2000);                                       //Tempo in millisecondi
      },
      (error) => {
        console.log(error)
      }
    )

  }

  modificaProdotto(){

    var prod = new Prodotto();
    prod.codice = this.prodottoCodice;
    prod.nome = this.prodottoNome;
    prod.descrizione = this.prodottoDescrizione
    prod.id = this.prodottoId;

    this.restProdotto.updateProdotto(prod).subscribe(
      (risultato) => {
        if(risultato){
          this.showTuttoOk = true;

          setTimeout(() => {
            this.router.navigateByUrl('prodotto/list')    //Redirect tramite URL
          }, 2000);                                       //Tempo in millisecondi
          
        }
      },
      (error) => {
        console.log(error)
      }
    )

  }

}
