import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProdottoDetailComponent } from './prodotto-detail/prodotto-detail.component';
import { ProdottoInsertComponent } from './prodotto-insert/prodotto-insert.component';
import { ProdottoListComponent } from './prodotto-list/prodotto-list.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "prodotto/list",
    pathMatch: "full"
  },
  {
    path: "prodotto/list",
    component: ProdottoListComponent
  },
  {
    path: "prodotto/insert",
    component: ProdottoInsertComponent
  },
  {
    path: "prodotto/detail/:identificatore",
    component: ProdottoDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
