import { Component, OnInit } from '@angular/core';
import { Prodotto } from '../prodotto';
import { ProdottoService } from '../prodotto.service';

@Component({
  selector: 'app-prodotto-insert',
  templateUrl: './prodotto-insert.component.html',
  styleUrls: ['./prodotto-insert.component.css']
})
export class ProdottoInsertComponent implements OnInit {

  prodottoNome: any;
  prodottoCodice: any;
  prodottoDescrizione: any;

  constructor(private restProdotto: ProdottoService) { }

  ngOnInit(): void {
  }

  inserisciProdotto(){
    var prod = new Prodotto();
    prod.codice = this.prodottoCodice;
    prod.nome = this.prodottoNome;
    prod.descrizione = this.prodottoDescrizione;

    this.restProdotto.insertProdotto(prod).subscribe(
      (risultato) => {                                //Success di AJAX
          alert("Inserimento effettuato con successo!")
      },
      (error) => {                                    //Error di AJAX
        console.log(error)
      }
    )
  }

}
