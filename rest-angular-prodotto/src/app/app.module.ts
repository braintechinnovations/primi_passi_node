import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProdottoDetailComponent } from './prodotto-detail/prodotto-detail.component';
import { ProdottoInsertComponent } from './prodotto-insert/prodotto-insert.component';
import { ProdottoListComponent } from './prodotto-list/prodotto-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ProdottoDetailComponent,
    ProdottoInsertComponent,
    ProdottoListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
