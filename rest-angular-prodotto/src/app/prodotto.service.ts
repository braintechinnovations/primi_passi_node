import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Prodotto } from './prodotto';

const endpoint = "http://localhost:8082/prodotto/";

@Injectable({
  providedIn: 'root'
})
export class ProdottoService {

  // private http : HttpClient | undefined;
  // constructor() { 
  //   this.http = new HttpClient();
  // }

  constructor(private http: HttpClient){

  }

  /**
   * Cerca un oggetto tramite il suo ID
   * @param varId identificatore univoco numerico
   * @returns Un oggetto di tipo Prodotto
   */
  findProdottoById(varId : number){
    console.log("-------> " + endpoint + varId);      //http://localhost:8082/prodotto/10
    return this.http.get<Prodotto>(endpoint + varId)
  }

  /**
   * Inserimento di un nuovo prodotto
   * @param objProdotto  Prodotto sotto forma di oggetto di tipo Prodotto
   * @returns Prodotto con il campo id riempito, altrimenti errore!
   */
  insertProdotto(objProdotto: Prodotto){
    let header_custom = new HttpHeaders();
    header_custom = header_custom.set('Content-type', 'application/json');

    return this.http.post(endpoint + "insert", JSON.stringify(objProdotto), { headers: header_custom })
  }

  /**
   * Ricerca di tutti i prodotti in DB
   * @returns Elenco di prodotti sotto forma di Array
   */
  findAllProdotti(){
    return this.http.get(endpoint)
  }

  /**
   * Eliminazione prodotto per ID
   * @param id Identificatore numerico dell'oggetto
   * @returns booleano con la effettiva riuscita dell'operazione
   */
  deleteProdotto(id: number){
    return this.http.delete<Boolean>(endpoint + id)
  }

  updateProdotto(objProdotto: Prodotto){
    let header_custom = new HttpHeaders();
    header_custom = header_custom.set('Content-type', 'application/json');

    return this.http.put<Boolean>(endpoint + "update", JSON.stringify(objProdotto), {headers: header_custom})
  }
}
